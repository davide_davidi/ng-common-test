#ng-common

Common module to start AngularJS applications.

Have a look at **[ng-boilerplate-app](https://github.com/mtfranchetto/ng-boilerplate-app)**.

##Contribute

I am using [Git Flow](https://github.com/nvie/gitflow).
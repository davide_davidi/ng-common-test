var Methods = {
    GET: 0,
    POST: 1,
    PUT: 2,
    DELETE: 3,
    JSONP: 4
};

module.exports = Methods;
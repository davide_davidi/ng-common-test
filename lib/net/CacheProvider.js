"use strict";

var Cache = function () {

};

Cache.prototype.setData = function () { };
Cache.prototype.getData = function () {return null; };
Cache.prototype.setExpireTime = function () { };
Cache.prototype.expire = function () { };
Cache.prototype.expireAll = function () { };

module.exports = Cache;